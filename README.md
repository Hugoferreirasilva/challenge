
# Desafio

### Tecnologias utilizadas

  - ElasticSearch
  - PostgreSQL
  - Node.js
  - PHP
  - ELK Stack
  - Kong | Open Source API Gateway


### Tecnologias utilizadas
  - Api gateway recebe todas as requisições e faz as devidas autenticações dos usuários
  - Serviços recebem as requisições que o gateway envia e consulta os dados nas respectivas bases de dados
  - Serviço de monitoramento(Packetbeat) captura as transações e envia para o Elasticsearch para ser vizualizado no Kibana